import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { Cache } from 'cache-manager';

import { cacheKeys } from '../../common/cache/cache.keys';
import { productsEventsKeys } from '../../common/events/products.events.keys';

@Injectable()
export class ProductsListener {
  constructor(@Inject(CACHE_MANAGER) private cacheService: Cache) {}

  @OnEvent(productsEventsKeys.productsUpdated)
  async productsUpdated() {
    await this.cacheService.del(cacheKeys.productsBackend);
    await this.cacheService.del(cacheKeys.productsFrontend);
  }
}
