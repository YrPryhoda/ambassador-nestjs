import { Inject, Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { MailerService } from '@nestjs-modules/mailer';

import { ordersEventsKeys } from '../../common/events/orders.events.keys';
import { CacheService } from '../../common/services/cache.service';
import { OrdersEntity } from '../../entities/orders/orders.entity';
import { cacheKeys } from '../../common/cache/cache.keys';
import { MailModule } from 'src/modules/mailer.module';
import { OrdersResponseDto } from 'src/dtos/orders/orders.response';

@Injectable()
export class OrdersListener {
  constructor(
    private readonly cacheService: CacheService,
    private readonly mailService: MailerService,
  ) {}

  @OnEvent(ordersEventsKeys.orderCompleted)
  async ordersCompleted(order: OrdersEntity) {
    const client = this.cacheService.getClient();

    client.zincrby(
      cacheKeys.rankings,
      order.ambasadorRevenue,
      `${order.user.firstName} ${order.user.lastName}`,
    );

    const respOrder = new OrdersResponseDto(order);

    await this.mailService.sendMail({
      to: 'admin@admin.com',
      subject: 'An order has been completed',
      html: `Order #${respOrder.id} with total of $${respOrder.totalSum} has been completed`,
    });

    await this.mailService.sendMail({
      to: respOrder.ambasadorEmail,
      subject: 'An order has been completed',
      html: `You earned #${order.ambasadorRevenue} from link #${order.code}`,
    });
  }
}
