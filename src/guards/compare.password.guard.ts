import { BadRequestException, CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { Request } from "express";

@Injectable()
export class ComparePasswordGuard implements CanActivate {
	canActivate(context: ExecutionContext) {
		const request = context.switchToHttp().getRequest<Request>();
		const { password, confirmPassword } = request.body;


		if (password !== confirmPassword) {
			throw new BadRequestException('Passwords are different');
		}

		return true

	}
}