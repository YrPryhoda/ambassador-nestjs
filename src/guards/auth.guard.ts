import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtService } from '@nestjs/jwt';

import { UsersService } from '../entities/users/users.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly jwtService: JwtService,
    @Inject(UsersService) private readonly usersService: UsersService,
  ) {}

  async canActivate(context: ExecutionContext) {
    try {
      const request = context.switchToHttp().getRequest<Request>();
      const jwt = request.cookies['jwt'];

      const { id, scope } = this.jwtService.verify(jwt);

      const user = await this.usersService.findOne({ where: { id } });

      return (
        (user.isAmbasador && scope === 'ambasador') ||
        (!user.isAmbasador && scope === 'admin')
      );
    } catch (error) {
      console.log(error);
      return undefined;
    }
  }
}
