import {
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  Column,
  Entity,
  Unique,
} from 'typeorm';

import { LinksEntity } from '../links/links.entity';
import { OrdersEntity } from '../orders/orders.entity';

@Entity('users')
@Unique('email', ['email'])
export class UsersEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column({ default: true })
  isAmbasador: boolean;

  @OneToMany(() => OrdersEntity, (orders) => orders.user, {
    createForeignKeyConstraints: false,
    eager: true
  })
  orders: OrdersEntity[];

  @OneToMany(() => LinksEntity, (links) => links.user, {
    nullable: true,
    eager: true
  })
  links: LinksEntity[];

  @CreateDateColumn({ update: false })
  createdAt: Date;

  @UpdateDateColumn({ update: true })
  updatedAt: Date;
}
