import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import { Request } from 'express';

import { BaseService } from '../shared/base.service';
import { UsersEntity } from './users.entity';
import { catchHandler } from '../../common/helpers/catch.handler';

@Injectable()
export class UsersService extends BaseService<UsersEntity> {
  constructor(
    @InjectRepository(UsersEntity)
    private readonly usersRepository: Repository<UsersEntity>,
    private readonly jwtService: JwtService,
  ) {
    super(usersRepository);
  }

  async user(request: Request) {
    try {
      const token = request.cookies['jwt'];

      const { id } = await this.jwtService.verifyAsync(token);

      return this.usersRepository.findOne({ where: { id } });
    } catch (error) {
      catchHandler(error);
    }
  }
}
