export interface UserInput {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  isAmbasador: boolean;
}
