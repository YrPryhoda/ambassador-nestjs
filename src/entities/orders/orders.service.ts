import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

import { BaseService } from '../shared/base.service';
import { OrdersEntity } from './orders.entity';

@Injectable()
export class OrdersService extends BaseService<OrdersEntity> {
  constructor(
    @InjectRepository(OrdersEntity) ordersRepository: Repository<OrdersEntity>,
  ) {
    super(ordersRepository);
  }
}
