import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { BaseService } from '../../shared/base.service';
import { OrderEntity } from './order.entity';

@Injectable()
export class OrderService extends BaseService<OrderEntity> {
  constructor(
    @InjectRepository(OrderEntity) orderRepository: Repository<OrderEntity>,
  ) {
    super(orderRepository);
  }
}
