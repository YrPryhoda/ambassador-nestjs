import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { OrdersEntity } from '../orders.entity';

@Entity('order')
export class OrderEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  productTitle: string;

  @Column()
  price: number;

  @Column()
  quantity: number;

  @Column()
  adminRevenue: number;

  @Column()
  ambasadorRevenue: number;

  @ManyToOne(() => OrdersEntity, (order) => order.orderItems)
  order: OrdersEntity;
}
