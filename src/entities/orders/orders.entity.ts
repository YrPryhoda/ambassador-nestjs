import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
  JoinColumn,
  JoinTable,
} from 'typeorm';

import { LinksEntity } from '../links/links.entity';
import { UsersEntity } from '../users/users.entity';
import { OrderEntity } from './order/order.entity';

@Entity('orders')
export class OrdersEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  transactionId: string;

  @Column()
  userId: number;

  @Column()
  code: string;

  @Column()
  ambasadorEmail: string;

  @Column()
  email: string;

  @Column({ nullable: true })
  address: string;

  @Column({ nullable: true })
  country: string;

  @Column({ nullable: true })
  city: string;

  @Column({ nullable: true })
  zip: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ default: false })
  complete: boolean;

  @CreateDateColumn({ update: false })
  createdAt: Date;

  @UpdateDateColumn({ update: true })
  updatedAt: Date;

  @ManyToOne(() => LinksEntity, (link) => link.orders, {
    createForeignKeyConstraints: false,
  })
  @JoinColumn({
    referencedColumnName: 'code',
    name: 'code',
  })
  link: LinksEntity;

  @ManyToOne(() => UsersEntity, (user) => user.orders, {
    createForeignKeyConstraints: false,
  })
  @JoinTable()
  user: UsersEntity;

  @OneToMany(() => OrderEntity, (item) => item.order, {
    cascade: true,
    eager: true,
  })
  orderItems: OrderEntity[];

  get ambasadorRevenue(): number {
    return this.orderItems.reduce((s, i) => s + i.ambasadorRevenue, 0)
}
}
