import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Unique,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { LinksEntity } from '../links/links.entity';

@Entity('products')
@Unique('title', ['title'])
export class ProductsEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  descriptions: string;

  @Column()
  image: string;

  @Column()
  price: number;

  @ManyToMany(() => LinksEntity, (links) => links.products, {
    nullable: true,
    eager: true,
  })
  @JoinTable()
  links: LinksEntity[];

  @CreateDateColumn({ update: false })
  createdAt: Date;

  @UpdateDateColumn({ update: true })
  updatedAt: Date;
}
