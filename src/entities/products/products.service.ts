import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { BaseService } from '../shared/base.service';
import { ProductsEntity } from './products.entity';

@Injectable()
export class ProductsService extends BaseService<ProductsEntity> {
  constructor(
    @InjectRepository(ProductsEntity)
    private readonly productRepository: Repository<ProductsEntity>,
  ) {
    super(productRepository);
  }
}
