import {
  DeepPartial,
  FindManyOptions,
  FindOneOptions,
  FindOptionsWhere,
  Repository,
} from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

export abstract class BaseService<T> {
  protected constructor(protected readonly repository: Repository<T>) {}

  async save(options: DeepPartial<T>) {
    return this.repository.save(options);
  }

  async findOne(otpions: FindOneOptions<T>) {
    return this.repository.findOne(otpions);
  }

  async findAll(options?: FindManyOptions<T>) {
    return this.repository.find(options);
  }

  async update(id: number, options: QueryDeepPartialEntity<T>) {
    return this.repository.update(id, options);
  }

  async delete(options: FindOptionsWhere<T>) {
    const deletedItem = await this.repository.findOne({ where: options });
    await this.repository.delete(options);

    return deletedItem;
  }
}
