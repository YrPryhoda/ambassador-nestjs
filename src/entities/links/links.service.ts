import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { BaseService } from '../shared/base.service';
import { LinksEntity } from './links.entity';

@Injectable()
export class LinksService extends BaseService<LinksEntity> {
  constructor(
    @InjectRepository(LinksEntity)
    readonly linksService: Repository<LinksEntity>,
  ) {
    super(linksService);
  }
}
