import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { LinksEntity } from './links.entity';
import { LinksService } from './links.service';

@Module({
  imports: [TypeOrmModule.forFeature([LinksEntity])],
  providers: [LinksService],
  exports: [LinksService],
})
export class LinksEntityModule {}
