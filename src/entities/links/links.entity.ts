import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  ManyToMany,
  OneToMany,
  JoinColumn,
  JoinTable,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

import { ProductsEntity } from '../products/products.entity';
import { OrdersEntity } from '../orders/orders.entity';
import { UsersEntity } from '../users/users.entity';

@Entity('links')
export class LinksEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  code: string;

  @ManyToOne(() => UsersEntity, (user) => user.links)
  user: UsersEntity;

  @ManyToMany(() => ProductsEntity, (products) => products.links)
  @JoinTable()
  products: ProductsEntity[];

  @OneToMany(() => OrdersEntity, (orders) => orders.link, {
    createForeignKeyConstraints: false,
    eager: true,
  })
  @JoinColumn({
    referencedColumnName: 'code',
    name: 'code',
  })
  orders: OrdersEntity[];

  @CreateDateColumn({
    update: false,
  })
  createdAt: Date;

  @UpdateDateColumn({ update: true })
  updatedAt: Date;
}
