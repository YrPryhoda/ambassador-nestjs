import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { Module } from '@nestjs/common';

import { TypeOrmConfigService } from './config/typeorm.config';
import { RedisGlobalModule } from './modules/redis.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { JwtGlobalModule } from './modules/jwt.module';
import { ApiModule } from './api/api.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      useClass: TypeOrmConfigService,
    }),
    JwtGlobalModule,
    RedisGlobalModule,
    EventEmitterModule.forRoot({
      global: true,
    }),
    ApiModule,
  ],
})
export class AppModule {}
