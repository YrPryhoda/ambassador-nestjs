import { CacheModule, Global, Module } from '@nestjs/common';
import * as redisStore from 'cache-manager-redis-store';
import { CacheService } from '../common/services/cache.service';

@Global()
@Module({
  imports: [
    CacheModule.register({
      store: redisStore,
      host: process.env.REDIS_HOST,
      port: 6379,
    }),
  ],
  providers: [CacheService],
  exports: [CacheModule, CacheService],
})
export class RedisGlobalModule {}
