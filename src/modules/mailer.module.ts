import { MailerModule, MailerService } from '@nestjs-modules/mailer';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    MailerModule.forRoot({
      transport: {
        host: 'localhost',
        port: 1025, 
      },
      defaults: {
        from: 'no-reply@ambasador.com',
      },
    }),
  ],
  exports: [MailerModule],
})
export class MailModule {}
