import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Stripe } from 'stripe';

@Injectable()
export class StripeService {
  private stripe: Stripe;

  constructor(private readonly configService: ConfigService) {
    this.stripe = new Stripe(this.configService.get('STRIPE_SECRET'), {
      apiVersion: '2020-08-27',
    });
  }

  async create(items: any) {
    return this.stripe.checkout.sessions.create({
      payment_method_types: ['card'],
      line_items: items,
      success_url: this.configService.get('STRIPE_SUCCESS'),
      cancel_url: this.configService.get('STRIPE_ERROR'),
    });
  }
}
