import * as bcrypt from 'bcrypt';


export class PasswordBcrypt {
	static async hash(password: string) {
		return await bcrypt.hash(password, 10);
	}

	static async compare(userPassword: string, hash: string) {
		return await bcrypt.compare(userPassword, hash);
	}
}