import { BadRequestException } from '@nestjs/common';

export const catchHandler = (error: Error): never => {
  console.log(error);
  throw new BadRequestException(error.message);
};
