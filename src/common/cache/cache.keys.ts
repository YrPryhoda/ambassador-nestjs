export const cacheKeys = {
  productsFrontend: 'products_frontend',
  productsBackend: 'products_backend',
  rankings: 'rankings',
};
