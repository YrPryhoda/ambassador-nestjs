import { NestFactory } from '@nestjs/core';
import * as casual from 'casual';
import { ProductsService } from '../../entities/products/products.service';
import { AppModule } from '../../app.module';
import * as coolImages from 'cool-images';

(async () => {
  const app = await NestFactory.createApplicationContext(AppModule);
  const productsService = app.get(ProductsService);

  const products = await productsService.findAll();

  if (products.length > 20) {
    process.exit();
  }

  for (let i = 0; i < 30; i++) {
    await productsService.save({
      title: casual.title,
      descriptions: casual.description,
      price: casual.integer(120, 1200),
      image: await coolImages.one(600, 800)
    });
  }

  await app.close();
})();
