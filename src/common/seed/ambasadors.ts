import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import * as casual from 'casual';
import { AppModule } from '../../app.module';
import { UsersService } from '../../entities/users/users.service';
import { PasswordBcrypt } from '../helpers/password.bcrypt';

(async () => {
  const app = await NestFactory.createApplicationContext(AppModule);
  const usersService = app.get(UsersService);
  const configService = app.get(ConfigService);

  const users = await usersService.findAll({ where: { isAmbasador: true } });

  if (users.length > 20) {
    process.exit();
  }

  const password = await PasswordBcrypt.hash(
    configService.get('AMBASADORS_PASSWORD'),
  );

  for (let i = 0; i < 30; i++) {
    await usersService.save({
      firstName: casual.first_name,
      lastName: casual.last_name,
      email: casual.email,
      password,
      isAmbasador: true,
    });
  }

  await app.close();
})();
