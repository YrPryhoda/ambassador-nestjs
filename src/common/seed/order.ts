import { NestFactory } from '@nestjs/core';
import * as casual from 'casual';
import { randomInt } from 'crypto';
import { OrdersService } from '../../entities/orders/orders.service';
import { UsersService } from '../../entities/users/users.service';
import { AppModule } from '../../app.module';
import { OrderService } from '../../entities/orders/order/order.service';

(async () => {
  const app = await NestFactory.createApplicationContext(AppModule);
  const orderService = app.get(OrderService);
  const ordersService = app.get(OrdersService);
  const usersService = app.get(UsersService);

  const users = await usersService.findAll({ where: { isAmbasador: true } });

  if (!users.length) {
    console.log('No ambasadors in DB found');
    return process.exit();
  }

  const idx: number[] = users.reduce((total, current) => {
    total.push(current.id);
    return total;
  }, []);
  const min = Math.min(...idx);
  const max = Math.max(...idx);

  const orders = await ordersService.findAll();

  if (orders.length > 20) {
    process.exit();
  }

  for (let i = 0; i < 30; i++) {
    const orders = await ordersService.save({
      userId: casual.integer(min, max),
      code: casual.words(2),
      ambasadorEmail: casual.email,
      email: casual.email,
      firstName: casual.first_name,
      lastName: casual.last_name,
      complete: true,
    });

    for (let i = 0; i < randomInt(1, 5); i++) {
      await orderService.save({
        order: orders,
        productTitle: casual.title,
        price: casual.integer(100, 1200),
        quantity: casual.integer(1, 5),
        adminRevenue: casual.integer(10, 100),
        ambasadorRevenue: casual.integer(1, 10),
      })
    }
  }

  await app.close();
})();
