import { NestFactory } from '@nestjs/core';

import { UserResponseDto } from '../../dtos/users/user.response.dto';
import { UsersService } from '../../entities/users/users.service';
import { CacheService } from '../services/cache.service';
import { cacheKeys } from '../cache/cache.keys';
import { AppModule } from '../../app.module';

(async () => {
  const app = await NestFactory.createApplicationContext(AppModule);
  const usersService = app.get(UsersService);

  const users = await usersService.findAll({
    where: { isAmbasador: true },
    relations: ['orders', 'orders.orderItems'],
  });

  const redisService = app.get(CacheService);
  const client = redisService.getClient();

  for (let i = 0; i < users.length; i++) {
    const user = new UserResponseDto(users[i]);
    await client.zadd(
      cacheKeys.rankings,
      user.revenue,
      `${user.firstName} ${user.lastName}`,
    );
  }

  process.exit();
})();
