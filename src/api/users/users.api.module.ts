import { Module } from '@nestjs/common';

import { UsersEntityModule } from '../../entities/users/users.module';
import { UsersController } from './users.controller';

@Module({
	imports: [UsersEntityModule],
	controllers: [UsersController],
})
export class UsersApiModule { }
