import {
  Body,
  Controller,
  Get,
  Put,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Request, Response } from 'express';

import { UserUpdatePasswordDto } from './../../dtos/users/user.update.password.dto';
import { ComparePasswordGuard } from './../../guards/compare.password.guard';
import { UserUpdateInfoDto } from '../../dtos/users/user.update.info.dto';
import { PasswordBcrypt } from './../../common/helpers/password.bcrypt';
import { UserResponseDto } from '../../dtos/users/user.response.dto';
import { CacheService } from '../../common/services/cache.service';
import { catchHandler } from '../../common/helpers/catch.handler';
import { UsersService } from '../../entities/users/users.service';
import { cacheKeys } from '../../common/cache/cache.keys';
import { AuthGuard } from './../../guards/auth.guard';

@UseGuards(AuthGuard)
@Controller('users')
export class UsersController {
  constructor(
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
    private readonly cacheService: CacheService,
  ) {}

  @Put('update')
  async updateInfo(@Body() body: UserUpdateInfoDto, @Req() request: Request) {
    try {
      const { id }: { id: number } = await this.jwtService.verifyAsync(
        request.cookies['jwt'],
      );
      const user = await this.usersService.findOne({ where: { id } });

      if (!user) {
        throw new Error('User does not exist');
      }

      await this.usersService.update(user.id, body);
      const updated = await this.usersService.findOne({ where: { id } });

      return new UserResponseDto(updated);
    } catch (error) {
      catchHandler(error);
    }
  }

  @UseGuards(ComparePasswordGuard)
  @Put('password')
  async updatePassword(
    @Body() body: UserUpdatePasswordDto,
    @Req() request: Request,
  ) {
    try {
      const { id }: { id: number } = await this.jwtService.verifyAsync(
        request.cookies['jwt'],
      );
      const user = await this.usersService.findOne({ where: { id } });

      if (!user) {
        throw new Error('User does not exist');
      }

      const hashedPassword = await PasswordBcrypt.hash(body.password);
      await this.usersService.update(user.id, { password: hashedPassword });
      const updated = await this.usersService.findOne({ where: { id } });

      return new UserResponseDto(updated);
    } catch (error) {
      catchHandler(error);
    }
  }

  @Get('ambasadors')
  async getAmbasadors() {
    try {
      const users = await this.usersService.findAll({
        where: { isAmbasador: true },
      });

      return users.map((user) => new UserResponseDto(user));
    } catch (error) {
      catchHandler(error);
    }
  }

  @Get('rankings')
  async rankings(@Res() response: Response) {
    try {
      const client = this.cacheService.getClient();
      client.zrevrangebyscore(
        cacheKeys.rankings,
        '+inf',
        '-inf',
        'withscores',
        (err: Error, res: string[]) => {
          if (err) {
            console.log(err);
            return;
          }

          const formattedCache = res.reduce((total, cur, index) => {
            if (!(index % 2)) {
              total.push({
                name: cur,
                ranking: res[index + 1],
              });
            }

            return total;
          }, []);

          return response.send(formattedCache);
        },
      );
    } catch (error) {
      catchHandler(error);
    }
  }
}
