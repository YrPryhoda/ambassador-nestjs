import { Response, Request } from 'express';
import { JwtService } from '@nestjs/jwt';
import {
  Body,
  Controller,
  Get,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';

import { ComparePasswordGuard } from './../../guards/compare.password.guard';
import { PasswordBcrypt } from './../../common/helpers/password.bcrypt';
import { UserResponseDto } from '../../dtos/users/user.response.dto';
import { catchHandler } from '../../common/helpers/catch.handler';
import { UsersService } from '../../entities/users/users.service';
import { AuthGuard } from './../../guards/auth.guard';
import { SignUpDto } from '../../dtos/auth/signup.dto';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  @UseGuards(ComparePasswordGuard)
  @Post(['signup', 'ambasador/signup'])
  async register(@Body() body: SignUpDto, @Req() request: Request) {
    try {
      const { confirmPassword, ...userToSave } = body;

      const userExist = await this.usersService.findOne({
        where: { email: body.email },
      });

      if (userExist) {
        throw new Error('This email is already registered in the system');
      }

      const hashed = await PasswordBcrypt.hash(body.password);

      const user = await this.usersService.save({
        ...userToSave,
        password: hashed,
        isAmbasador: request.path === '/api/auth/ambasador/signup',
      });

      return new UserResponseDto(user);
    } catch (error) {
      catchHandler(error);
    }
  }

  @Post('signin')
  async signin(
    @Body('email') email: string,
    @Body('password') password: string,
    @Res({ passthrough: true }) response: Response,
  ) {
    try {
      const user = await this.usersService.findOne({ where: { email } });

      if (!user) {
        throw new Error('User does not exist');
      }

      const isEquals = await PasswordBcrypt.compare(password, user.password);

      if (!isEquals) {
        throw new Error('Invalid credentials');
      }

      const jwt = await this.jwtService.signAsync({
        scope: user.isAmbasador ? 'ambasador' : 'admin',
        id: user.id,
      });

      response.cookie('jwt', jwt, { httpOnly: true });

      return new UserResponseDto(user);
    } catch (error) {
      catchHandler(error);
    }
  }

  @UseGuards(AuthGuard)
  @Get('login')
  async login(@Req() request: Request) {
    try {
      const { id }: { id: number } = await this.jwtService.verifyAsync(
        request.cookies['jwt'],
      );

      const user = await this.usersService.findOne({ where: { id } });

      if (!user) {
        throw new Error('User does not exist');
      }

      return new UserResponseDto(user);
    } catch (error) {
      catchHandler(error);
    }
  }

  @UseGuards(AuthGuard)
  @Get('logout')
  async logout(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');

    return response.status(200);
  }
}
