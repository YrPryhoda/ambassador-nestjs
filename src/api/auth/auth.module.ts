import { Module } from '@nestjs/common';
import { UsersEntityModule } from '../../entities/users/users.module';
import { AuthController } from './auth.controller';

@Module({
	imports: [
		UsersEntityModule,
	],
	controllers: [AuthController],
})
export class AuthApiModule { }
