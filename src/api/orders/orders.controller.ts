import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { DataSource } from 'typeorm';

import { ProductsService } from '../../entities/products/products.service';
import { ordersEventsKeys } from '../../common/events/orders.events.keys';
import { OrderService } from '../../entities/orders/order/order.service';
import { OrderEntity } from '../../entities/orders/order/order.entity';
import { OrdersResponseDto } from '../../dtos/orders/orders.response';
import { OrdersCreateDto } from '../../dtos/orders/orders.create.dto';
import { OrdersService } from '../../entities/orders/orders.service';
import { StripeService } from '../../modules/stripe/stripe.service';
import { OrdersEntity } from '../../entities/orders/orders.entity';
import { LinksService } from '../../entities/links/links.service';
import { catchHandler } from '../../common/helpers/catch.handler';
import { AuthGuard } from '../../guards/auth.guard';

@UseGuards(AuthGuard)
@Controller('orders')
export class OrdersController {
  constructor(
    private readonly ordersService: OrdersService,
    private readonly linksService: LinksService,
    private readonly productsService: ProductsService,
    private readonly orderItemService: OrderService,
    private readonly stripeService: StripeService,
    private readonly eventEmitter: EventEmitter2,
    private dataSource: DataSource,
  ) {}

  @Get()
  async getAll() {
    try {
      const orders = await this.ordersService.findAll();

      return orders.map((el) => new OrdersResponseDto(el));
    } catch (error) {
      catchHandler(error);
    }
  }

  @Post()
  async create(@Body() body: OrdersCreateDto) {
    try {
      const link = await this.linksService.findOne({
        relations: ['user'],
        where: {
          code: body.code,
        },
      });

      if (!link) {
        throw new Error('Invalid link');
      }

      const queryRunner = this.dataSource.createQueryRunner();
      await queryRunner.connect();
      await queryRunner.startTransaction();

      try {
        const order = new OrdersEntity();
        order.userId = link.user.id;
        order.ambasadorEmail = link.user.email;
        order.firstName = body.firstName;
        order.lastName = body.lastName;
        order.email = body.email;
        order.address = body.address;
        order.country = body.country;
        order.city = body.city;
        order.zip = body.zip;
        order.code = body.code;

        const savedOrders = await queryRunner.manager.save(order);

        const lineItems = [];

        for (let item of body.orderItems) {
          const product = await this.productsService.findOne({
            where: { id: item.id },
          });

          const orderItem = new OrderEntity();
          orderItem.order = savedOrders;
          orderItem.productTitle = product.title;
          orderItem.price = product.price;
          orderItem.quantity = item.quantity;
          orderItem.ambasadorRevenue = 0.1 * product.price * item.quantity;
          orderItem.adminRevenue = 0.9 * product.price * item.quantity;

          await queryRunner.manager.save(orderItem);

          lineItems.push({
            currency: 'usd',
            name: product.title,
            description: product.descriptions,
            images: [product.image],
            amount: product.price * 100,
            quantity: item.quantity,
          });
        }

        const source = await this.stripeService.create(lineItems);

        order.transactionId = source['id'];
        await queryRunner.manager.save(order);

        await queryRunner.commitTransaction();
        return source;
      } catch (error) {
        await queryRunner.rollbackTransaction();
        return catchHandler(error);
      } finally {
        await queryRunner.release();
      }
    } catch (error) {
      catchHandler(error);
    }
  }

  @Post('confirm')
  async confirm(@Body('source') source: string) {
    try {
      const order = await this.ordersService.findOne({
        relations: ['orderItems', "user"],
        where: {
          transactionId: source,
        },
      });

      if (!order) {
        throw Error('Order not found');
      }

      await this.ordersService.update(order.id, { complete: true });

      this.eventEmitter.emit(ordersEventsKeys.orderCompleted, order);

      return {
        message: "success"
      }
    } catch (error) {
      catchHandler(error);
    }
  }
}
