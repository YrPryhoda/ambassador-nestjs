import { Module } from '@nestjs/common';

import { ProductsEntityModule } from '../../entities/products/products.module';
import { OrderEntityModule } from '../../entities/orders/order/order.module';
import { OrdersEntityModule } from '../../entities/orders/orders.module';
import { OrdersListener } from '../../listeners/orders/orders.listener';
import { UsersEntityModule } from '../../entities/users/users.module';
import { LinksEntityModule } from '../../entities/links/links.module';
import { StripeModule } from '../../modules/stripe/stripe.module';
import { OrdersController } from './orders.controller';
import { MailModule } from '../../modules/mailer.module';

@Module({
  imports: [
    OrderEntityModule,
    OrdersEntityModule, 
    UsersEntityModule,
    LinksEntityModule,
    ProductsEntityModule,
    StripeModule,
    MailModule,
  ],
  controllers: [OrdersController],
  providers: [OrdersListener],
})
export class OrdersApiModule {}
