import { Module } from '@nestjs/common';

import { ProductsEntityModule } from '../../entities/products/products.module';
import { ProductsListener } from '../../listeners/products/products.listener';
import { UsersEntityModule } from '../../entities/users/users.module';
import { ProductsController } from './products.controller';

@Module({
  imports: [ProductsEntityModule, UsersEntityModule],
  controllers: [ProductsController],
  providers: [ProductsListener],
})
export class ProductsApiModule {}
