import {
  Body,
  CacheInterceptor,
  CacheKey,
  CacheTTL,
  CACHE_MANAGER,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Request } from 'express';
import { Cache } from 'cache-manager';
import { EventEmitter2 } from '@nestjs/event-emitter';

import { productsEventsKeys } from '../../common/events/products.events.keys';
import { ProductsCreateDto } from '../../dtos/products/products.create.dto';
import { ProductsService } from '../../entities/products/products.service';
import { ProductsEntity } from '../../entities/products/products.entity';
import { catchHandler } from '../../common/helpers/catch.handler';
import { cacheKeys } from '../../common/cache/cache.keys';
import { AuthGuard } from '../../guards/auth.guard';


@Controller('products')
export class ProductsController {
  constructor(
    private readonly productsService: ProductsService,
    @Inject(CACHE_MANAGER) private cacheService: Cache,
    private readonly eventEmmiter: EventEmitter2,
  ) {}

  @Get()
  async findAll() {
    try {
      return this.productsService.findAll();
    } catch (error) {
      catchHandler(error);
    }
  }

  @CacheKey(cacheKeys.productsFrontend)
  @CacheTTL(1800)
  @UseInterceptors(CacheInterceptor)
  @Get('frontend')
  async frontend() {
    try {
      return await this.productsService.findAll();
    } catch (error) {
      catchHandler(error);
    }
  }

  @Get('backend')
  async backend(@Req() request: Request) {
    try {
      let products = await this.cacheService.get<ProductsEntity[]>(
        cacheKeys.productsBackend,
      );
      const sortType = request.query.sort?.toString().toLocaleLowerCase();
      const s = request.query.s?.toString().toLocaleLowerCase();
      const page = +request.query.page || 1;
      const perPage = 9;

      if (!products) {
        products = await this.productsService.findAll();
        await this.cacheService.set(cacheKeys.productsBackend, products, {
          ttl: 1800,
        });
      }

      if (s) {
        const filterPattern = (item: ProductsEntity, key: string) =>
          item[key].toLocaleLowerCase().indexOf(s) >= 0;

        products = products.filter(
          (el) =>
            filterPattern(el, 'title') || filterPattern(el, 'descriptions'),
        );
      }

      if (sortType) {
        products.sort((a, b) =>
          sortType === 'asc' ? a.price - b.price : b.price - a.price,
        );
      }

      const total = products.length;
      const data = products.slice((page - 1) * perPage, perPage * page);

      return {
        data,
        total,
        page,
        last_page: Math.ceil(total / perPage),
      };
    } catch (error) {
      catchHandler(error);
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: number) {
    try {
      const product = await this.productsService.findOne({ where: { id } });

      if (!product) {
        throw new Error('Product was not found');
      }
      return product;
    } catch (error) {
      catchHandler(error);
    }
  }

  @UseGuards(AuthGuard)
  @Post()
  async create(@Body() body: ProductsCreateDto) {
    try {
      const existedProduct = await this.productsService.findOne({
        where: { title: body.title },
      });

      if (existedProduct) {
        throw new Error('Product with such title has already existed');
      }

      const product = await this.productsService.save(body);
      this.eventEmmiter.emit(productsEventsKeys.productsUpdated, product);
      return product;
    } catch (error) {
      catchHandler(error);
    }
  }

  @UseGuards(AuthGuard)
  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() body: Partial<ProductsCreateDto>,
  ) {
    try {
      await this.productsService.update(id, body);

      const product = await this.productsService.findOne({ where: { id } });

      this.eventEmmiter.emit(productsEventsKeys.productsUpdated, product);

      return product;
    } catch (error) {
      catchHandler(error);
    }
  }

  @UseGuards(AuthGuard)
  @Delete(':id')
  async delete(@Param('id') id: number) {
    try {
      const product = await this.productsService.delete({ id });
      this.eventEmmiter.emit(productsEventsKeys.productsUpdated, product);

      return product;
    } catch (error) {
      catchHandler(error);
    }
  }
}
