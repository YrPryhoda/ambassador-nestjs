import {
  Body,
  Controller,
  Get,
  Param,
  ParseArrayPipe,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { v4 } from 'uuid';

import { UserResponseDto } from '../../dtos/users/user.response.dto';
import { LinkResponseDto } from '../../dtos/links/link.response.dto';
import { UsersService } from '../../entities/users/users.service';
import { catchHandler } from '../../common/helpers/catch.handler';
import { LinksService } from '../../entities/links/links.service';
import { AuthGuard } from '../../guards/auth.guard';

@UseGuards(AuthGuard)
@Controller('links')
export class LinksController {
  constructor(
    private readonly linksService: LinksService,
    private readonly usersService: UsersService,
  ) {}

  @Get('stats')
  async stats(@Req() request: Request) {
    try {
      const user = await this.usersService.user(request);

      const links = await this.linksService.findAll({
        relations: ['user', 'orders'],
        where: {
          user: { id: user.id },
          orders: { complete: true },
        },
      });

      return links.map((link) => ({
        ...link,
        user: new UserResponseDto(link.user),
      }));
    } catch (error) {
      catchHandler(error);
    }
  }

  @Get('checkout/:code')
  async checkout(@Param('code') code: string) {
    try {
      const link = await this.linksService.findOne({
        where: { code },
        relations: ['user', 'products'],
      });

      return new LinkResponseDto(link);
    } catch (error) {
      catchHandler(error);
    }
  }

  @Get(':userId')
  async getAll(@Param('userId') userId: number) {
    try {
      const links = await this.linksService.findAll({
        relations: ['user', 'orders'],
        where: {
          user: { id: userId },
        },
      });

      return links.map((link) => new LinkResponseDto(link));
    } catch (error) {
      catchHandler(error);
    }
  }

  @Post()
  async create(
    @Body('productsId', new ParseArrayPipe({ expectedType: Number }))
    productsId: number[],
    @Req() request: Request,
  ) {
    try {
      const user = await this.usersService.user(request);

      return this.linksService.save({
        code: v4(),
        user,
        products: productsId.map((id) => ({ id })),
      });
    } catch (error) {
      catchHandler(error);
    }
  }
}
