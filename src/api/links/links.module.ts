import { Module } from '@nestjs/common';
import { UsersEntityModule } from '../../entities/users/users.module';
import { LinksEntityModule } from '../../entities/links/links.module';
import { LinksController } from './links.controller';

@Module({
  imports: [LinksEntityModule, UsersEntityModule],
  controllers: [LinksController],
  providers: [],
})
export class LinksApiModule {}
