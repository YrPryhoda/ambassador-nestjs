import { Module } from '@nestjs/common';

import { AuthApiModule } from './auth/auth.module';
import { LinksApiModule } from './links/links.module';
import { OrdersApiModule } from './orders/orders.module';
import { ProductsApiModule } from './products/products.module';
import { UsersApiModule } from './users/users.api.module';

@Module({
  imports: [
    AuthApiModule,
    UsersApiModule,
    ProductsApiModule,
    OrdersApiModule,
    LinksApiModule,
  ],
})
export class ApiModule {}
