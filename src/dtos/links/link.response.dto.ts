import { LinksEntity } from '../../entities/links/links.entity';
import { ProductsEntity } from '../../entities/products/products.entity';
import { OrdersResponseDto } from '../orders/orders.response';
import { UserResponseDto } from '../users/user.response.dto';

export class LinkResponseDto {
  readonly id: number;
  readonly code: string;
  readonly createdAt: Date;
  readonly updatedAt: Date;
  readonly user: UserResponseDto;
  readonly products: ProductsEntity[];
  readonly orders: OrdersResponseDto[];

  constructor(link: LinksEntity) {
    this.id = link.id;
    this.code = link.code;
    this.products = link.products;
    this.createdAt = link.createdAt;
    this.updatedAt = link.updatedAt;
    this.orders = link.orders?.map((el) => new OrdersResponseDto(el));
    this.user = new UserResponseDto(link.user)
  }
}
