import { IsNotEmpty } from "class-validator";

export class ProductsCreateDto {
    @IsNotEmpty()
    title: string;

    @IsNotEmpty()
    descriptions: string;

    @IsNotEmpty()
    image: string;

    @IsNotEmpty()
    price: number;
}