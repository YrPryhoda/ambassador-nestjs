import { LinksEntity } from 'src/entities/links/links.entity';
import { UsersEntity } from '../../entities/users/users.entity';
import { OrdersResponseDto } from '../orders/orders.response';

export class UserResponseDto {
  readonly id: number;
  readonly email: string;
  readonly firstName: string;
  readonly lastName: string;
  readonly isAmbasador: boolean;
  readonly createdAt: Date;
  readonly updatedAt: Date;
  readonly revenue: number;
  readonly orders: OrdersResponseDto[];
  readonly links: LinksEntity[];

  constructor(user: UsersEntity) {
    this.id = user.id;
    this.email = user.email;
    this.firstName = user.firstName;
    this.lastName = user.lastName;
    this.isAmbasador = user.isAmbasador;
    this.createdAt = user.createdAt;
    this.updatedAt = user.updatedAt;
    this.links = user.links;
    
    if (this.isAmbasador) {
      this.orders = user.orders.map((el) => new OrdersResponseDto(el));

      this.revenue = user.orders
        .filter((order) => order.complete)
        .reduce((total, curr) => {
          let itemSumm = 0;
          if (curr.orderItems?.length) {
            curr.orderItems.forEach((el) => (itemSumm += el.ambasadorRevenue));
          }
          return (total += itemSumm);
        }, 0);
    }
  }
}
