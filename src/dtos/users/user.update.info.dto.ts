import { IsEmail, IsNotEmpty } from "class-validator";

export class UserUpdateInfoDto {
	@IsNotEmpty()
	firstName: string;

	@IsNotEmpty()
	@IsEmail()
	email: string;

	@IsNotEmpty()
	lastName: string;
}