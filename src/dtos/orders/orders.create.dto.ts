import { IsEmail, IsNotEmpty } from 'class-validator';

export class OrdersCreateDto {
  @IsNotEmpty()
  firstName: string;

  @IsNotEmpty()
  lastName: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  address: string;

  @IsNotEmpty()
  country: string;

  @IsNotEmpty()
  city: string;

  @IsNotEmpty()
  zip: string;

  @IsNotEmpty()
  code: string;

  @IsNotEmpty()
  orderItems: {
    id: number;
    quantity: number;
  }[];
}
