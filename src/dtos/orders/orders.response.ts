import { OrdersEntity } from '../../entities/orders/orders.entity';
import { OrderEntity } from '../../entities/orders/order/order.entity';

export class OrdersResponseDto {
  readonly id: number;
  readonly transactionId: string;
  readonly userId: number;
  readonly code: string;
  readonly ambasadorEmail: string;
  readonly email: string;
  readonly address: string;
  readonly country: string;
  readonly city: string;
  readonly zip: string;
  readonly fullName: string;
  readonly createdAt: Date;
  readonly updatedAt: Date;
  readonly orderItems: OrderEntity[];
  readonly totalSum: number;

  constructor(orders: OrdersEntity) {
    this.id = orders.id;
    this.transactionId = orders.transactionId;
    this.userId = orders.userId;
    this.code = orders.code;
    this.ambasadorEmail = orders.ambasadorEmail;
    this.email = orders.email;
    this.address = orders.address;
    this.country = orders.country;
    this.city = orders.city;
    this.zip = orders.zip;
    this.fullName = `${orders.firstName} ${orders.lastName}`;
    this.createdAt = orders.createdAt;
    this.updatedAt = orders.updatedAt;
    this.totalSum = orders.orderItems?.reduce((total, curr) => {
      return (total += curr.adminRevenue);
    }, 0);
    this.orderItems = orders.orderItems;
  }
}
