import { IsEmail, IsNotEmpty } from "class-validator";
import { SignUpUserInput } from "./interfaces/auth.interfaces";

export class SignUpDto implements SignUpUserInput {
    @IsNotEmpty()
    firstName: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    lastName: string;

    @IsNotEmpty()
    password: string;

    @IsNotEmpty()
    confirmPassword: string;
}